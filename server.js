const express = require('express');
var app = express();
const https = require('https');
var fs = require('fs-extra');
const device = require('express-device');
const bodyParser = require("body-parser");
const PORT = process.env.PORT || 3000;
const MobileDetect = require('mobile-detect');

app.use(express.static("docs"), express.json(), device.capture(),express.urlencoded({extended: true}), bodyParser.json(), bodyParser.urlencoded({extended: true}));

app.get("/", async (req, res) => {
    res.sendFile(__dirname + "/docs/index.html");
});

var secureServer = https.createServer({
    key: fs.readFileSync('privkey.pem'),
    cert: fs.readFileSync('fullchain.pem')
}, app);

app.use(function (req, response, next) {

    response.setHeader("Access-Control-Allow-Origin", "");
    response.setHeader("Access-Control-Allow-Credentials", "true");
    response.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    response.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");

    // Pass to next layer of middleware
    next();
});

secureServer.listen(PORT, () => {
    console.log("Server started on port: "+ PORT);
})
// const io = require('socket.io')(secureServer);

app.get("/getDevice", async(req, res) => {
    res.send(req.device.type);
})

app.get("/isIphone", async(req, res) => {
    var md = new MobileDetect(req.headers['user-agent']);
    res.send(md.is('iphone'));
})

app.get("/get360ProductImagery", async (req, res) => {
    var files = [];
    // var folder = req.query.Folder;
    var folder = "./docs"+req.query.Folder;
    var subFolderCount = parseInt(req.query.SubfolderCount);
    var fileName = req.query.Filename;
    var fileCount = parseInt(req.query.FileCount);
    var FileExt = req.query.FileExt;
    for (var a = 0; a < subFolderCount; a++) {
        files[a] = new Array();
        for (var b = 1; b <= fileCount; b++) {
            var id = 0;
            if (b < 10) {
                id = "0"+b;
            }   else    {
                id = b;
            }
            let file = fs.readFileSync(folder+a+"/"+fileName+id+"."+FileExt);
            var data = file.toString('base64');
            files[a].push({Filename:fileName+id+"."+FileExt, Data: data, Url: ""});
        }
    }
    res.send(files);
})

app.post("/getPano", async(req, res) => {
    try {
        var a = req.body;
        a.forEach((item, index) => {
            let file = fs.readFileSync("./docs/"+item.data.Url);
            let data = file.toString('base64');
            item.data.Url = 'data:image/png;base64,'+data;
        });
        res.status(200).send(a);
    }   catch (err) {
        res.status(400).send(err.Message);
        // console.log(err);
    }
})

// app.post("/UpdatePano", async(req, res) => {
//     try {
//         var a = req.body;
//         a.Pano.forEach((item, index) => {
//             item.Updated = false;
//         });
//         fs.writeFile("./docs/js/temp.js", 'var MasterData='+JSON.stringify(a)+'', function(err) {
//             if (err) {
//                 console.log(err);
//             }   else    {
//                 console.log("Done Writing the file!");res.send('ok!');
//             }
//         });
//         res.status(200).send(a);
//     }   catch (err) {
//         // res.status(400).send(err.Message);
//         console.log(err);
//     }
// })

app.post("/SaveHotspotDetails", async (req, res) => {
    try {
        var init = "var MasterData = ";
        fs.appendFile("./docs/js/temp.js", init+JSON.stringify(req.body), function(err) {
            if (err) {
                console.log(err);
            }   else    {
                console.log("Done Writing the file!");res.send('ok!');
            }
        });
    } catch (err) {
        res.status(400).send(err.Message);
    }
})