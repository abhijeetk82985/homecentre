var app = angular.module('HomeCentre', ['ui.router']);

app.service('xrNotify', function() {
    this.create = function(data) {
        var xrNotify = $(".xrNotify");
        xrNotify.html(data.message);
        xrNotify.css({
            "opacity": 1
        });
        setTimeout(() => {
            xrNotify.css("opacity", 0);
            setTimeout(() => {
                xrNotify[0].classList.remove("xrsuccess", "xrerror");
                xrNotify.html("");
            }, 300);
        }, 3000);
        if (data.status == 200) {
            xrNotify[0].classList.add("xrSuccess");
        }
        if (data.status == 404) {
            xrNotify[0].classList.add("xrError");
        }
        if (data.status == 300) {
            xrNotify[0].classList.add("xrNeutral");
        }
    }
})

app.run( function($rootScope, $http) {
    $http.get("/getDevice")
    .then( function(res) {
        $rootScope.deviceType = res.data;
    })
    $rootScope.MasterData = MasterData;
    $rootScope.appState = "pano";

    window.addEventListener("deviceorientation", handleOrientation, true);
    function handleOrientation() {
        if (window.matchMedia("(orientation: portrait)").matches) {
            $(".pano_control").css("display", 'block');
            $(".splash_logo").css({
                "width": "50vw",
                "margin": "0"
            })
            $(".loading_spinner").css("margin-left", "19vw");
            $(".pano_options").css("width", "30px");
            $(".pano_control").css("margin-left", "0");
            $(".mobile_panel_img").removeClass('mobile_panel_img_landscape');
            $(".classic_button").removeClass('classic_button_landscape');
            $(".product_details").css({
                'padding': '10vw',
            });
            $(".product_content").css({
                'width': '80vw',
                'height': 'calc(100vh - 20vw)'
            })
        }
        
        if (window.matchMedia("(orientation: landscape)").matches) {
            $(".pano_control").css("display", 'inline');
            $(".splash_logo").css({
                "width": "30vw",
                "margin": "0 10vw"
            })
            $(".loading_spinner").css("margin-left", "23.5vw");
            $(".pano_options").css("width", "unset");
            $(".pano_control").css("margin-left", "15px");
            $(".mobile_panel_img").addClass('mobile_panel_img_landscape');
            $(".classic_button").addClass('classic_button_landscape');
            $(".product_details").css({
                'padding': '5vw'
            });
            $(".product_content").css({
                'width': '90vw',
                'height': 'calc(100vh - 10vw)'
            })
        }
    }

    $http.get("/isIphone")
    .then(function(res) {
        $rootScope.isIphone = res.data;
        if (!res.data) {
            screenfull.on('change', () => {
                if (screenfull.isFullscreen) {
                    $(".fullscreen")[0].innerText = "fullscreen_exit";
                }   else    {
                    $(".fullscreen")[0].innerText = "fullscreen";
                }
            });
        
            $rootScope.fullscreen = function() {
                var toggle = parseInt(event.target.dataset.toggle);
                if (!toggle) {
                    screenfull.request();
                    event.target.dataset.toggle = 1;
                }   else    {
                    screenfull.exit();
                    event.target.dataset.toggle = 0;
                }
            }
        }
    })
})

app.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: './templates/home.html',
            controller: "home_ctrl"  
        })

    $urlRouterProvider.otherwise("/home");

});

app.controller('home_ctrl', function($rootScope, $scope, $state, $http, $compile, xrNotify) {

    var Animationduration = 1000;
    var start = true;
    InitiatePano(0);
    $(".loading_spinner").velocity({opacity: 1}, {duration: 10});
    setTimeout(() => {
        $(".splash_screen").velocity({opacity: 0}, {duration: 250});
        setTimeout(() => {
            $(".splash_screen").css("display", "none");
            $(".pano_options").show();
            $(".header").show();
        }, 10);
    }, 1500);

    function GetImgData(x) {
        var j = $rootScope.MasterData.Pano[x];
        var z = [];
        switch(start) {
            case true: 
            $http.post("/getPano", Isolate({data: $rootScope.MasterData, array: [0]}))
            .then(function(res) {
                assemble(res.data);
            });
            start = false;
            j.Updated = true;
            case false:
                for (var a = 0; a <= j.InteractiveObjects.length; a++) {
                    if (a < j.InteractiveObjects.length) {
                        if ((j.InteractiveObjects[a].Type == 'Panorama') || (j.InteractiveObjects[a].Type == 'Interaction')){
                            if (!$rootScope.MasterData.Pano[parseInt(j.InteractiveObjects[a].Target)].Updated) {
                                z.push(parseInt(j.InteractiveObjects[a].Target));
                            }
                        }
                    }   else    {
                        getPano();
                    }
                    
                };
        }
        function getPano() {
            if (z.length) {
                $http.post("/getPano", Isolate({data: $rootScope.MasterData, array: z}))
                .then( function(res) {
                    assemble(res.data);
                })
            }
        }
        
        function Isolate(object, boolean = true) {
            var data = [];
            for (var b = 0; b < object.array.length; b++) {
                $rootScope.MasterData.Pano[object.array[b]].Updated = boolean;
                var insert = {
                    data: object.data.Pano[object.array[b]],
                    index: object.array[b]
                }
                data.push(insert);
            }
            return data;
        }

        function assemble(array) {
            array.forEach(element => {
                $rootScope.MasterData.Pano[element.index].Url = element.data.Url;
                console.log()
            });
        }
    }

    var scene, material, camera, deviceOrientationControls, OrbitControls, renderer, canvas, gyroEnabled = false;

    function InitiatePano(x) {
        $rootScope.appState = "pano", $scope.target;
        createThreeScene();
        CreateCamera();
        // GetImgData(18);
        CreatePano(x);
        CreateRenderer();
        CreateHotspots(x);
        CreateOrbitControls();
        CreateZoomControls();
        CreateDeviceOrientationControls();
    }

    function createThreeScene() {
        scene = new THREE.Scene();
    }
    function CreateCamera() {
        camera = new THREE.PerspectiveCamera(65, window.innerWidth/ window.innerHeight, 0.1, 1000);
    }
    function CreateOrbitControls() {
        OrbitControls = new THREE.OrbitControls(camera, renderer.domElement);
        camera.position.set( 0.1, 0, 0 );
        OrbitControls.update();
        OrbitControls.target = new THREE.Vector3(0,0,0);
        OrbitControls.enableZoom = false;
        OrbitControls.enableDamping = true;
    }
    function CreateDeviceOrientationControls() {
        deviceOrientationControls = new THREE.DeviceOrientationControls(camera);
        deviceOrientationControls.enabled = false;
    }
    function CreateZoomControls() {
        camera.target = new  THREE.Vector3(0,0,0);
        canvas = $(".canvas")[0];
        canvas.addEventListener( 'wheel', onDocumentMouseWheel, false );

        var hammertime = new Hammer(canvas);
        hammertime.get('pinch').set({ enable: true });
        hammertime.on('pinch', zoom);
        
        function zoom(e) {
            event.preventDefault();
            var deltaY = (e.scale < 1.0)?2*e.scale:-2*e.scale;
            var fov = camera.fov + deltaY*2.5 * 0.05;
            camera.fov = THREE.MathUtils.clamp( fov, 10, 75 );
            camera.updateProjectionMatrix();
        };

        function onDocumentMouseWheel( event ) {
            event.preventDefault();
            var fov = camera.fov + event.deltaY * 0.05;
            camera.fov = THREE.MathUtils.clamp( fov, 10, 75 );
            camera.updateProjectionMatrix();
        }
        
        function animate() {
            requestAnimationFrame( animate );
            if (gyroEnabled) {
                deviceOrientationControls.update();
            }   else    {
                OrbitControls.update();
            }
            renderer.render( scene, camera );
        }
        animate();
    }

    function CreatePano(x) {
        var geometry = new THREE.SphereBufferGeometry( 500, 60, 40 );
        geometry.scale( - 1, 1, 1 );
        var texture = new THREE.TextureLoader().load($rootScope.MasterData.Pano[x].Url);
        texture.minFilter = THREE.LinearFilter;
        material = new THREE.MeshBasicMaterial( { map: texture } );
        var equirect = new THREE.Mesh( geometry, material );
        scene.add( equirect );
    }

    function CreateRenderer() {
        renderer = new THREE.WebGLRenderer({antialias: true});
        renderer.setSize(window.innerWidth, window.innerHeight);
        $(".canvas").append(renderer.domElement);
        window.addEventListener( 'resize', onWindowResize, false );
        function onWindowResize() {
            camera.aspect = window.innerWidth / window.innerHeight;
            camera.updateProjectionMatrix();
            renderer.setSize( window.innerWidth, window.innerHeight );
        }
    }

    function CreateHotspots(x) {
        var Hotspots = $rootScope.MasterData.Pano[x].InteractiveObjects, Mesh = [];
        for( var c = 0; c < $rootScope.MasterData.Pano[x].InteractiveObjects.length; c++) {
            const a = c;
            var Geometry, Texture, Material;
            if (Hotspots[a].Type == "Panorama") {
                Geometry = new THREE.Geometry();
                var ArrowVerticesArray = [[0,0,1], [2,0,-1], [1,0,-1], [0,0,0], [-1,0,-1], [-2,0,-1]], V = [];
                for (var b = 0; b < 6; b++) {
                    V.push(new THREE.Vector3(ArrowVerticesArray[b][0], ArrowVerticesArray[b][1], ArrowVerticesArray[b][2]));
                    Geometry.vertices.push(V[b]);
                }
                Geometry.faces.push( new THREE.Face3( 0, 1, 2 ));
                Geometry.faces.push( new THREE.Face3( 0, 2, 3));
                Geometry.faces.push( new THREE.Face3( 0, 3, 4 ));
                Geometry.faces.push( new THREE.Face3( 0, 4, 5 ));
                Material = new THREE.MeshBasicMaterial( {color: 0xc1d82f , side: THREE.DoubleSide, transparent: false});
            }   else    {
                Texture = new THREE.TextureLoader().load( Hotspots[a].Texture );
                Material = new THREE.MeshBasicMaterial( {map: Texture, side: THREE.DoubleSide, transparent: false} );
                Geometry = new THREE.CircleBufferGeometry( 2, 20 );
            }

            Mesh[a] = new THREE.Mesh( Geometry, Material );
            (Hotspots[a].Type == "Panorama")?Mesh[a].name = "Navigation: "+Hotspots[a].Name :Mesh[a].name = "Information"+Hotspots[a].Name;
            
            Mesh[a].position.set(Hotspots[a].Coordinates.x, Hotspots[a].Coordinates.y, Hotspots[a].Coordinates.z);
            Mesh[a].quaternion.set(Hotspots[a].Quaternion._x, Hotspots[a].Quaternion._y, Hotspots[a].Quaternion._z, Hotspots[a].Quaternion._w);
            Mesh[a].scale.set(Hotspots[a].Scale.x, Hotspots[a].Scale.y, Hotspots[a].Scale.z);
            Mesh[a].userData = Hotspots[a];
            scene.add(Mesh[a]);
        }
        
        $scope.enableHotspotToggleBtn = function() {
            var action = parseInt(event.target.dataset.toggle);
            if (!action) {
                event.target.dataset.toggle = 1;
                event.target.innerText = 'radio_button_checked';
                for (var a = 0; a < Mesh.length; a++) {
                    Mesh[a].visible = false;
                }
            }   else    {
                event.target.dataset.toggle = 0;
                event.target.innerText = 'radio_button_unchecked';
                for (var a = 0; a < Mesh.length; a++) {
                    Mesh[a].visible = true;
                }
            }
        }

        function click() {
            var raycaster = new THREE.Raycaster();
            var mouse = new THREE.Vector2();
            mouse.x = (event.clientX / renderer.domElement.clientWidth) * 2 - 1;
            mouse.y =  - (event.clientY / renderer.domElement.clientHeight) * 2 + 1;
            raycaster.setFromCamera(mouse, camera);
            var intersects = raycaster.intersectObjects(Mesh, true);
            if (intersects.length) {
                var Hotspot = intersects[0].object.userData;
                var AlignCameraY = function(y, camera_) {
                    camera_.rotation.y = y;
                }

                if ((Hotspot.Type == "Panorama") || (Hotspot.Type == "Interaction")) {
                    $(".spinner").show(50);
                    var mat = new THREE.TextureLoader().load($rootScope.MasterData.Pano[Hotspot.Target].Url);
                    GetImgData(parseInt(Hotspot.Target));
                    mat.minFilter = THREE.LinearFilter;

                    setTimeout(() => {
                        material.map = mat;
                        $(".spinner").hide(100);
                        removeHotspots();
                        CreateHotspots(Hotspot.Target);
                    }, 1000);
                }

                if (Hotspot.Type == "Information") {
                    $(".product_details").css("display", 'block');
                    $scope.CurrentProduct = $rootScope.MasterData.Models[parseInt(Hotspot.Target)];
                    $scope.$apply();
                    $(".model")[0].innerHTML = "<model-viewer src="+$rootScope.MasterData.Models[parseInt(Hotspot.Target)].ModelURL.glb+" ios-src="+$rootScope.MasterData.Models[parseInt(Hotspot.Target)].ModelURL.usdz+" shadow-intensity='1' ar ar-scale='fixed' ar ar-modes='webxr scene-viewer quick-look' ar-scale='auto' camera-controls alt="+$rootScope.MasterData.Models[parseInt(Hotspot.Target)].Alias+" poster="+$rootScope.MasterData.Models[parseInt(Hotspot.Target)].ImagesPath+"></model-viewer>";
                }
            }
        }

        function removeHotspots() {
            for (var a = 0; a < Mesh.length; a++) {
                scene.remove(Mesh[a]);
            }
            Mesh = [];
        }

        $(".canvas").on('mousedown', click);
    }

    $scope.Gyro = function() {
        if (gyroEnabled) {
            deviceOrientationControls.enabled = false;
            OrbitControls.enabled = true;
            gyroEnabled = false;
            $(".togglegyro")[0].innerText = '3d_rotation';
        }   else    {
            gyroEnabled = true;
            OrbitControls.enabled = false;
            deviceOrientationControls.enabled = true;
            $(".togglegyro")[0].innerText = 'pan_tool';
        }
    }

    $scope.close_model = function() {
        $(".model")[0].innerHTML = "";
        $(".product_details").css("display", 'none');
        $scope.CurrentProduct = undefined;
    }

    $scope.Cart = {
        Total: 0
    }

    $scope.AddToCart = function() {
        $scope.CurrentProduct.Quantity += 1;
        calcPrice();
        xrNotify.create({
            message: 'Added '+$scope.CurrentProduct.Name+' to the cart',
            status: 200
        });
        $scope.popup.Cart += 1;
        console.log($scope.popup);
    }

    function calcPrice() {
        var a = 0;
        for (var c = 0; c < $rootScope.MasterData.Models.length; c++) {
            var b = $rootScope.MasterData.Models[c];
            a += b.Quantity*(b.Price - (b.PercentageDiscount*b.Price)/100);
        }
        $scope.Cart.Total = a;
    }

    $scope.CloseCart = function() {
        $(".cart").css('display', 'none');
    }

    $scope.open_cart = function() {
        $(".cart").css("display", 'block');
        $scope.popup.Cart = 0;
        console.log($scope.popup);

    }

    $scope.add_remove = function(action) {
        console.log(action);
        if (action == 'add') {
            this.item.Quantity-=1;
        }   else    {
            this.item.Quantity+=1;
        }
        calcPrice();
    }

    $scope.popup = {
        Cart: 0
    }

});