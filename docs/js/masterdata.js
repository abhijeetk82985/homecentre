var MasterData = {
    "Pano": [{
        "Name": "scene_1_00",
        "Url": "./images/360/scene_1_00.tif.jpg",
        "Updated": false,
        "North": 1.5707963267948966,
        "InteractiveObjects": [{
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": -77.29007005879056,
                "y": -63.37668131679413,
                "z": -3.1050500119584896
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.6989771737230053,
                "_z": 0,
                "_w": -0.7151439789400451
            },
            "Scale": {
                "x": 5.23,
                "y": 5.23,
                "z": 5.23
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "1"
        }, {
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": -51.94754277063316,
                "y": -58.077801688928965,
                "z": -62.67712302806089
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.9229081075621682,
                "_z": 0,
                "_w": -0.38502029166787727
            },
            "Scale": {
                "x": 4.9,
                "y": 4.9,
                "z": 4.9
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "2"
        }, {
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": -8.886732432107909,
                "y": -57.83676091874,
                "z": -81.89324323778709
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.998656800905955,
                "_z": 0,
                "_w": -0.05181306789106228
            },
            "Scale": {
                "x": 4.4,
                "y": 4.4,
                "z": 4.4
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "3",
            "Position": {
                "x": -8.886732432107909,
                "y": -57.83676091874,
                "z": -81.89324323778709
            }
        }, {
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": 59.42669340068058,
                "y": -59.68348386147557,
                "z": -42.721927896270465
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.8882567361023695,
                "_z": 0,
                "_w": 0.45934733129600885
            },
            "Scale": {
                "x": 4.9,
                "y": 4.9,
                "z": 4.9
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "5",
            "Position": {
                "x": 59.42669340068058,
                "y": -59.68348386147557,
                "z": -42.721927896270465
            }
        }, {
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": 72.79537693220084,
                "y": -61.57655930469366,
                "z": 30.152287500193857
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.5641026369702105,
                "_z": 0,
                "_w": 0.8257046778135964
            },
            "Scale": {
                "x": 5.42,
                "y": 5.42,
                "z": 5.42
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "6"
        }]
    }, {
        "Name": "scene_1_01",
        "Url": "./images/360/scene_1_01.tif.jpg",
        "Updated": false,
        "North": 1.5707963267948966,
        "InteractiveObjects": [{
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": -17.25585484471382,
                "y": -64.58994799351713,
                "z": -79.85446373898945
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.9905334367505378,
                "_z": 0,
                "_w": -0.13727166743056754
            },
            "Scale": {
                "x": 4.65,
                "y": 4.65,
                "z": 4.65
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "2",
            "Position": {
                "x": -17.25585484471382,
                "y": -64.58994799351713,
                "z": -79.85446373898945
            }
        }, {
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": 79.03161850643932,
                "y": -61.21119934384278,
                "z": 2.681855913538588
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.7328992229692087,
                "_z": 0,
                "_w": 0.6803372170998219
            },
            "Scale": {
                "x": 5.82,
                "y": 5.82,
                "z": 5.82
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "0"
        }, {
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": 43.313382401499354,
                "y": -64.20616692754429,
                "z": -63.257561077027034
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.9688214399870653,
                "_z": 0,
                "_w": 0.2477599996395489
            },
            "Scale": {
                "x": 4.93,
                "y": 4.93,
                "z": 4.93
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "3"
        }, {
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": -8.026119931856392,
                "y": -61.664119775466034,
                "z": 76.94038747002348
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0,
                "_z": 0,
                "_w": 1
            },
            "Scale": {
                "x": 4.34,
                "y": 4.34,
                "z": 4.34
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "8",
            "Position": {
                "x": -8.026119931856392,
                "y": -61.664119775466034,
                "z": 76.94038747002348
            }
        }]
    }, {
        "Name": "scene_1_02",
        "Url": "./images/360/scene_1_02.tif.jpg",
        "Updated": false,
        "North": 0.7652597489513508,
        "InteractiveObjects": [{
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": 16.998023800046976,
                "y": -64.4638906538635,
                "z": -69.63810306236601
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.9939609554551797,
                "_z": 0,
                "_w": 0.10973431109104514
            },
            "Scale": {
                "x": 5.13,
                "y": 5.13,
                "z": 5.13
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "3",
            "Position": {
                "x": 16.998023800046976,
                "y": -64.4638906538635,
                "z": -69.63810306236601
            }
        }, {
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": 60.29732730637356,
                "y": -53.92335534296395,
                "z": 67.54591486087052
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.386791437380836,
                "_z": 0,
                "_w": 0.9221672212613431
            },
            "Scale": {
                "x": 5.05,
                "y": 5.05,
                "z": 5.05
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "1",
            "Position": {
                "x": 60.29732730637356,
                "y": -53.92335534296395,
                "z": 67.54591486087052
            }
        }, {
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": 78.55486818372383,
                "y": -60.18587921807451,
                "z": 14.380286067569036
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.7389847086700415,
                "_z": 0,
                "_w": 0.6737221982032757
            },
            "Scale": {
                "x": 6.51,
                "y": 6.51,
                "z": 6.51
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "0"
        }, {
            "Name": "",
            "Type": "Information",
            "Text": "",
            "Coordinates": {
                "x": -99.66254629938973,
                "y": -7.471311572644346,
                "z": -4.539465914885225
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.7440783833509366,
                "_z": 0,
                "_w": 0.6680923285219317
            },
            "Scale": {
                "x": 1.29,
                "y": 1.29,
                "z": 1.29
            },
            "Texture": "./icons/info.svg",
            "Target": "10",
            "Position": {
                "x": -99.66254629938973,
                "y": -7.471311572644346,
                "z": -4.539465914885225
            }
        }]
    }, {
        "Name": "scene_1_03",
        "Url": "./images/360/scene_1_03.tif.jpg",
        "Updated": false,
        "North": 0.06224600835701478,
        "InteractiveObjects": [{
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": -23.833832197082742,
                "y": -68.72705067252026,
                "z": -71.23254114952178
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.9982398279237653,
                "_z": 0,
                "_w": -0.05930637357596165
            },
            "Scale": {
                "x": 4.44,
                "y": 4.44,
                "z": 4.44
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "4",
            "Position": {
                "x": -23.833832197082742,
                "y": -68.72705067252026,
                "z": -71.23254114952178
            }
        }, {
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": -25.413753534926403,
                "y": -70.66771156123187,
                "z": 65.25873091439902
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.08724267680304644,
                "_z": 0,
                "_w": -0.9961870885251621
            },
            "Scale": {
                "x": 4.78,
                "y": 4.78,
                "z": 4.78
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "2",
            "Position": {
                "x": -25.413753534926403,
                "y": -70.66771156123187,
                "z": 65.25873091439902
            }
        }, {
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": 84.09777487092315,
                "y": -63.571431249282675,
                "z": -7.593814302635179
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.7271140840906946,
                "_z": 0,
                "_w": 0.6865166485358898
            },
            "Scale": {
                "x": 6.03,
                "y": 6.03,
                "z": 6.03
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "0",
            "Position": {
                "x": 84.09777487092315,
                "y": -63.571431249282675,
                "z": -7.593814302635179
            }
        }, {
            "Name": "",
            "Type": "Information",
            "Text": "",
            "Coordinates": {
                "x": -96.44674055882513,
                "y": 16.067058754851285,
                "z": -4.305192614716014
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.7328992229692087,
                "_z": 0,
                "_w": 0.6803372170998219
            },
            "Scale": {
                "x": 1.34,
                "y": 1.34,
                "z": 1.34
            },
            "Texture": "./icons/info.svg",
            "Target": "5",
            "Position": {
                "x": -96.44674055882513,
                "y": 16.067058754851285,
                "z": -4.305192614716014
            }
        }]
    }, {
        "Name": "scene_1_04",
        "Url": "./images/360/scene_1_04.tif.jpg",
        "Updated": false,
        "North": -1.5195349082048961,
        "InteractiveObjects": [{
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": 11.683339260551254,
                "y": -59.128253687100994,
                "z": -86.31288590574823
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.9999496433554291,
                "_z": 0,
                "_w": 0.010035474744629437
            },
            "Scale": {
                "x": 5.91,
                "y": 5.91,
                "z": 5.91
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "0",
            "Position": {
                "x": 11.683339260551254,
                "y": -59.128253687100994,
                "z": -86.31288590574823
            }
        }, {
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": 72.25654327629306,
                "y": -68.94499299227397,
                "z": -5.05765707185475
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.7442532640123749,
                "_z": 0,
                "_w": 0.6678975063637581
            },
            "Scale": {
                "x": 5.27,
                "y": 5.27,
                "z": 5.27
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "3"
        }, {
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": -41.87796057947863,
                "y": -59.772959231224334,
                "z": -68.36248797729664
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.9454901608179105,
                "_z": 0,
                "_w": -0.3256506652787951
            },
            "Scale": {
                "x": 5.89,
                "y": 5.89,
                "z": 5.89
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "5"
        }]
    }, {
        "Name": "scene_1_05",
        "Url": "./images/360/scene_1_05.tif.jpg",
        "Updated": false,
        "North": "",
        "InteractiveObjects": [{
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": 73.81464368596411,
                "y": -67.15362429079082,
                "z": -18.91654272803769
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.7090786361789602,
                "_z": 0,
                "_w": 0.7051294120334124
            },
            "Scale": {
                "x": 5.13,
                "y": 5.13,
                "z": 5.13
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "6",
            "Position": {
                "x": 73.81464368596411,
                "y": -67.15362429079082,
                "z": -18.91654272803769
            }
        }, {
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": 31.83540034746682,
                "y": -68.70654857842098,
                "z": 65.31399135834212
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.16934950384902459,
                "_z": 0,
                "_w": 0.9855560590580777
            },
            "Scale": {
                "x": 5.72,
                "y": 5.72,
                "z": 5.72
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "0"
        }, {
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": -60.21468800146434,
                "y": -65.21090307731721,
                "z": 63.42429054740381
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.47884508758591177,
                "_z": 0,
                "_w": -0.8778994145657237
            },
            "Scale": {
                "x": 6.32,
                "y": 6.32,
                "z": 6.32
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "4",
            "Position": {
                "x": -60.21468800146434,
                "y": -65.21090307731721,
                "z": 63.42429054740381
            }
        }, {
            "Name": "",
            "Type": "Information",
            "Text": "",
            "Coordinates": {
                "x": -99.48908104150125,
                "y": -10.094420682356324,
                "z": 0.15945094923420486
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.7386318472696628,
                "_z": 0,
                "_w": 0.6741090373218606
            },
            "Scale": {
                "x": 1.34,
                "y": 1.34,
                "z": 1.34
            },
            "Texture": "./icons/info.svg",
            "Target": "6"
        }]
    }, {
        "Name": "scene_1_06",
        "Url": "./images/360/scene_1_06.tif.jpg",
        "Updated": false,
        "North": -0.062246008287867895,
        "InteractiveObjects": [{
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": -57.09226211318342,
                "y": -69.5910730448546,
                "z": -43.560947639660846
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.9268893532997766,
                "_z": 0,
                "_w": -0.3753346862994704
            },
            "Scale": {
                "x": 5.37,
                "y": 5.37,
                "z": 5.37
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "9"
        }, {
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": 18.30619241000109,
                "y": -75.61520065292018,
                "z": -73.20880249481644
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.9996293644209887,
                "_z": 0,
                "_w": 0.027223772466175567
            },
            "Scale": {
                "x": 6.03,
                "y": 6.03,
                "z": 6.03
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "0",
            "Position": {
                "x": 18.30619241000109,
                "y": -75.61520065292018,
                "z": -73.20880249481644
            }
        }, {
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": 78.38030896535686,
                "y": -61.21498953524808,
                "z": -10.452378805548028
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.7210954859107013,
                "_z": 0,
                "_w": 0.6928356949517032
            },
            "Scale": {
                "x": 5.28,
                "y": 5.28,
                "z": 5.28
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "5"
        }, {
            "Name": "",
            "Type": "Information",
            "Text": "",
            "Coordinates": {
                "x": -97.85214629690945,
                "y": -29.401424413692077,
                "z": -0.35587740429559184
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.7090786361789602,
                "_z": 0,
                "_w": 0.7051294120334124
            },
            "Scale": {
                "x": 1.97,
                "y": 1.97,
                "z": 1.97
            },
            "Texture": "./icons/info.svg",
            "Target": "9",
            "Position": {
                "x": -97.85214629690945,
                "y": -29.401424413692077,
                "z": -0.35587740429559184
            }
        }]
    }, {
        "Name": "scene_1_07",
        "Url": "./images/360/scene_1_07.tif.jpg",
        "Updated": false,
        "North": -0.09153824748217787,
        "InteractiveObjects": [{
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": -21.653937100377814,
                "y": -61.121376291857565,
                "z": 76.12676512398271
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.052945974508877267,
                "_z": 0,
                "_w": -0.9985973782177205
            },
            "Scale": {
                "x": 4.78,
                "y": 4.78,
                "z": 4.78
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "9"
        }, {
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": -5.376191324593065,
                "y": -68.62854694236677,
                "z": -78.42703673934527
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.9997057387743591,
                "_z": 0,
                "_w": -0.024257696956077875
            },
            "Scale": {
                "x": 5.68,
                "y": 5.68,
                "z": 5.68
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "8",
            "Position": {
                "x": -5.376191324593065,
                "y": -68.62854694236677,
                "z": -78.42703673934527
            }
        }, {
            "Name": "",
            "Type": "Information",
            "Text": "",
            "Coordinates": {
                "x": -95.60174682181804,
                "y": -35.057462508011966,
                "z": -0.453814343267792
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.6969148113750006,
                "_z": 0,
                "_w": 0.7171539204983456
            },
            "Scale": {
                "x": 1.59,
                "y": 1.59,
                "z": 1.59
            },
            "Texture": "./icons/info.svg",
            "Target": "3",
            "Position": {
                "x": -95.60174682181804,
                "y": -35.057462508011966,
                "z": -0.453814343267792
            }
        }, {
            "Name": "",
            "Type": "Information",
            "Text": "",
            "Coordinates": {
                "x": -80.65257397405297,
                "y": -56.064424150140624,
                "z": -18.760134756260292
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.6718498738846352,
                "_z": 0,
                "_w": 0.7406873476448749
            },
            "Scale": {
                "x": 1.98,
                "y": 1.98,
                "z": 1.98
            },
            "Texture": "./icons/info.svg",
            "Target": "4"
        }, {
            "Name": "",
            "Type": "Information",
            "Text": "",
            "Coordinates": {
                "x": 93.94789605066975,
                "y": -30.115751433097422,
                "z": 0.9615341450048156
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.7298640726978355,
                "_z": 0,
                "_w": -0.6835923020228714
            },
            "Scale": {
                "x": 1.86,
                "y": 1.86,
                "z": 1.86
            },
            "Texture": "./icons/info.svg",
            "Target": "0",
            "Position": {
                "x": 93.94789605066975,
                "y": -30.115751433097422,
                "z": 0.9615341450048156
            }
        }]
    }, {
        "Name": "scene_1_08",
        "Url": "./images/360/scene_1_08.tif.jpg",
        "Updated": false,
        "North": "",
        "InteractiveObjects": [{
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": 52.73902598700569,
                "y": -61.490688881962534,
                "z": -48.416809024995246
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.9440026752312568,
                "_z": 0,
                "_w": 0.3299377958892104
            },
            "Scale": {
                "x": 5.86,
                "y": 5.86,
                "z": 5.86
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "1",
            "Position": {
                "x": 52.73902598700569,
                "y": -61.490688881962534,
                "z": -48.416809024995246
            }
        }, {
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": 53.97666754600424,
                "y": -64.96103748513079,
                "z": 53.54047972780561
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.4096842273084465,
                "_z": 0,
                "_w": 0.9122274025124882
            },
            "Scale": {
                "x": 6.16,
                "y": 6.16,
                "z": 6.16
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "7"
        }, {
            "Name": "",
            "Type": "Information",
            "Text": "",
            "Coordinates": {
                "x": -74.30998616895279,
                "y": 14.342881746111683,
                "z": -65.36289236858404
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.3673941944765065,
                "_z": 0,
                "_w": 0.9300653234396813
            },
            "Scale": {
                "x": 1.34,
                "y": 1.34,
                "z": 1.34
            },
            "Texture": "./icons/info.svg",
            "Target": "1"
        }, {
            "Name": "",
            "Type": "Information",
            "Text": "",
            "Coordinates": {
                "x": -98.117611637951,
                "y": -15.531438376726376,
                "z": -1.3086747028685244
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.7328992229692087,
                "_z": 0,
                "_w": 0.6803372170998219
            },
            "Scale": {
                "x": 1.54,
                "y": 1.54,
                "z": 1.54
            },
            "Texture": "./icons/info.svg",
            "Target": "11",
            "Position": {
                "x": -98.117611637951,
                "y": -15.531438376726376,
                "z": -1.3086747028685244
            }
        }, {
            "Name": "",
            "Type": "Information",
            "Text": "",
            "Coordinates": {
                "x": -71.10846994047587,
                "y": -14.454282307456506,
                "z": 67.43168051354638
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.9277060000770342,
                "_z": 0,
                "_w": 0.3733116357965149
            },
            "Scale": {
                "x": 1.49,
                "y": 1.49,
                "z": 1.49
            },
            "Texture": "./icons/info.svg",
            "Target": "7",
            "Position": {
                "x": -71.10846994047587,
                "y": -14.454282307456506,
                "z": 67.43168051354638
            }
        }]
    }, {
        "Name": "scene_1_09",
        "Url": "./images/360/scene_1_09.tif.jpg",
        "Updated": false,
        "North": -1.4527196961468296,
        "InteractiveObjects": [{
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": 74.54422094090533,
                "y": -66.36693937942222,
                "z": 6.211962791387732
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.7262148132107448,
                "_z": 0,
                "_w": 0.6874678502106721
            },
            "Scale": {
                "x": 5.03,
                "y": 5.03,
                "z": 5.03
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "7"
        }, {
            "Name": "",
            "Type": "Panorama",
            "Text": "",
            "Coordinates": {
                "x": -25.891640941017066,
                "y": -61.77539791758787,
                "z": 69.09722815068865
            },
            "Quaternion": {
                "_x": 0,
                "_y": 0.43302990852511414,
                "_z": 0,
                "_w": -0.9013795528647914
            },
            "Scale": {
                "x": 6.35,
                "y": 6.35,
                "z": 6.35
            },
            "Texture": "./icons/pano_inner_blue.svg",
            "Target": "6",
            "Position": {
                "x": -25.891640941017066,
                "y": -61.77539791758787,
                "z": 69.09722815068865
            }
        }]
    }],
    "Logo": "./icons/HC logo-01.svg",
    "Models": [{
        "Alias": "Sofa",
        "Name": "Left Side Corner Sofa",
        "Type": "Furniture",
        "Color": "   ",
        "ModelURL": {
            "glb": "./models/sofa.glb",
            "usdz": "./models/sofa.usdz"
        },
        "Price": 46875,
        "PercentageDiscount": 60,
        "ImagesPath": "./images/products/sofa.jpg",
        "Quantity": 0,
        "Specification": ["Seat Cushion : Fixed", "Frame Material : Solid Wood", "Design : Textured", "Upholstery Material : Polyester", "Color Shade : Beige", "Seating Capacity : Left Lounge", "Seat Filling : Foam and Fibre", "Storage Availability : No", "Type : Corner Sofas", "Assembly Required : Yes"],
        "Description": "Designed from fine quality wood with a laid back contemporary structure and plush seating, this corner sofa set makes for a durable and versatile seating arrangement."
    }, {
        "Alias": "Bookcase",
        "Name": "Bookcase",
        "Type": "Furniture",
        "Color": "   ",
        "ModelURL": {
            "glb": "./models/book_shelf.glb",
            "usdz": "./models/book_shelf.usdz"
        },
        "Price": 13200,
        "PercentageDiscount": 35,
        "ImagesPath": "./images/products/book_shelf.jpg",
        "Quantity": 0,
        "Specification": ["Assembly Type : Assembely Service Provided by Retailer", "Storage Availability : Yes", "Type : Open Book Shelves", "Assembly Required : Yes", "Style : Contemporary", "Primary Material : Compressed Wood", "Color Shade : Rich Brown"],
        "Description": "Made from compressed wood, this bookshelf is designed with multiple compartments. A classy addition to your living space, this shelf is an ideal pick to place your decorative items and books."
    }, {
        "Alias": "Table",
        "Name": "Study Table",
        "Type": "Furniture",
        "Color": "   ",
        "ModelURL": "",
        "Price": 6000,
        "PercentageDiscount": 45,
        "ImagesPath": "  ",
        "Quantity": 0,
        "Specification": [],
        "Description": ""
    }, {
        "Alias": "TV Stand",
        "Name": "TV Stand",
        "Type": "Furniture",
        "Color": "   ",
        "ModelURL": {
            "glb": "./models/tv_stand.glb",
            "usdz": "./models/tv_stand.usdz"
        },
        "Price": 42800,
        "PercentageDiscount": 25,
        "ImagesPath": "./images/products/tv_stand.jpg",
        "Quantity": 0,
        "Specification": ["Assembly Type : Assembly Service Provided by Retailer", "Style : Contemporary", "Primary Material : Solid Wood", "Color Shade : Rich Dark", "Installation Type : Free Standing", "Storage Type : Drawer Storage", "Type : Low TV Units", "Assembly Required : Yes"],
        "Description": "With multiple storage units and shelves, you can share all your media units in one place with this TV unit from HOME CENTRE. A standout piece, it will add to the aesthetic value of your living room."
    }, {
        "Alias": "Coffee Table",
        "Name": "Coffee Table",
        "Type": "Furniture",
        "Color": "   ",
        "ModelURL": {
            "glb": "./models/coffee_table.glb",
            "usdz": "./models/coffee_table.usdz"
        },
        "Price": 14200,
        "PercentageDiscount": 25,
        "ImagesPath": "./images/products/coffee_table.jpg",
        "Quantity": 0,
        "Specification": ["Assembly Type : Assembly Service Provided by Retailer", "Primary Material : Compressed Wood", "Type : Coffee Tables", "Assembly Required : Yes", "Style : Contemporary", "Finish : Veneer", "Color Shade : Rich Brown", "Shape : Rectangular"],
        "Description": "Add a hint of finesse to your living room decor with this classy coffee table that has been crafted from fine quality wood and brings about a stylish yet convenient vibe to your room."
    }, {
        "Alias": "Wardrobe",
        "Name": "Wardrobe",
        "Type": "Furniture",
        "Color": "   ",
        "ModelURL": {
            "glb": "./models/wardrobe.glb",
            "usdz": "./models/wardrobe.usdz"
        },
        "Price": 29875,
        "PercentageDiscount": 60,
        "ImagesPath": "./images/products/wardrobe.jpg",
        "Quantity": 0,
        "Specification": ["Assembly Type : Assembly Service Provided by Retailer", "Storage Availability : Yes", "Finish : Melamine", "Color Shade : Walnut", "With Mirror : No", "Assembly Required : Yes", "Style : Contemporary", "Primary Material : Engineered Wood", "Type : Hinged", "Door Type : 3 Door", "Adjustable Shelves : No"],
        "Description": "Say bye to those utility woes with this compact wardrobe. Designed with multiple shelves, this one shall be a perfect add-on to your furniture collection."
    }, {
        "Alias": "Bed",
        "Name": "Bed",
        "Type": "Furniture",
        "Color": "   ",
        "ModelURL": {
            "glb": "./models/bed.glb",
            "usdz": "./models/bed.glb"
        },
        "Price": 70600,
        "PercentageDiscount": 60,
        "ImagesPath": "./images/products/bed.jpg",
        "Quantity": 0,
        "Specification": ["Recommended Mattress Size : 150 x 195 cm", "Storage Type : Hydraulic Box", "Type : Queen Bed", "Headboard Type : With Storage", "Storage Availability : Yes", "Finish : Melamine", "Color Shade : Walnut", "Assembly Required : Yes"],
        "Description": "Isn't it amazing when a truly refreshing style and sheer versatility hits your personal space? This queen-size bed with hydraulic storage and modish headboard offers enhanced convenience and a striking visual aid."
    }, {
        "Alias": "Drawer Table",
        "Name": "Drawer Table",
        "Type": "Furniture",
        "Color": "   ",
        "ModelURL": {
            "glb": "./models/drawer.glb",
            "usdz": "./models/drawer.usdz"
        },
        "Price": 40000,
        "PercentageDiscount": 45,
        "ImagesPath": "./images/products/drawer.jpg",
        "Quantity": 0,
        "Specification": ["Assembly Type : Assembly Service Provided by Retailer", "Storage Availability : Yes", "Primary Material : Compressed Wood", "Finish : Veneer", "Assembly Required : Yes", "No. of Doors : 2", "Style : Contemporary", "Storage Type : Drawer Storage", "Wood Type : Rubber Wood", "Color Shade : Rich Brown", "Shape : Rectangular"],
        "Description": "We proudly present this versatile drawer chest with a stunning construction and enhanced storage space that promise to light up your personal living space."
    }, {
        "Alias": "Standing Mirror",
        "Name": "Standing Mirror",
        "Type": "Furniture",
        "Color": "   ",
        "ModelURL": "https://www.cgtrader.com/3d-models/interior/bathroom/ikea-ikornnes",
        "Price": 7200,
        "PercentageDiscount": 25,
        "ImagesPath": "  ",
        "Quantity": 0,
        "Specification": [],
        "Description": ""
    }, {
        "Alias": "Dining Table",
        "Name": "Dining Table",
        "Type": "Furniture",
        "Color": "   ",
        "ModelURL": {
            "glb": "./models/dining_table.glb",
            "usdz": "./models/dining_table.usdz"
        },
        "Price": 68800,
        "PercentageDiscount": 61,
        "ImagesPath": "./images/products/dining_table.jpg",
        "Quantity": 0,
        "Specification": ["Seating Capacity : 8 Seater", "Style : Contemporary", "Finish : Veneer", "Color Shade : Rich Dark", "Shape : Rectangular", "Seat Type : Chairs", "Primary Material : Compressed Wood", "Type : Dining Tables Sets", "Assembly Required : Yes"],
        "Description": "Made of high quality compressed wood for durability, this 6-seater dining table set features thick section legs and cushioned seats that offer extra stability and enough space to accommodate your entire family."
    }, {
        "Alias": "Bar Chair",
        "Name": "Bar Chair",
        "Type": "Furniture",
        "Color": "   ",
        "ModelURL": {
            "glb": "./models/bar_chair.glb",
            "usdz": "./models/bar_chair.usdz"
        },
        "Price": 13200,
        "PercentageDiscount": 35,
        "ImagesPath": "./images/products/bar_chair.jpg",
        "Quantity": 0,
        "Specification": ["Assembly Type : Assembly Service Provided by Retailer", "Style : Contemporary", "Upholstery Material : Polyester", "Finish : Chrome", "Color Shade : Grey", "Adjustable Height : Yes", "Primary Material : Metal", "Additional Feature : Wood Type: Solid Wood", "Type : Bar Chairs", "Assembly Required : Yes"],
        "Description": "Slender and elegant in form , this bar stool from HOME CENTRE comes with an adjustable gas lift and a footrest which will make getting on and off easier."
    }, {
        "Alias": "Arm Chair",
        "Name": "Arm Chair",
        "Type": "Furniture",
        "Color": "   ",
        "ModelURL": {
            "glb": "./models/arm_chair.glb",
            "usdz": "./models/arm_chair.usdz"
        },
        "Price": 60800,
        "PercentageDiscount": 35,
        "ImagesPath": "./images/products/arm_chair.jpg",
        "Quantity": 0,
        "Specification": ["Seat Cushion : Fixed", "Frame Material : Solid Wood", "Design : Solid", "Storage Availability : No", "Upholstery Material : Leather", "Color Shade : Tan", "Seating Capacity : One Seater", "Seat Filling : Foam with Spring", "Style : Contemporary", "Wood Type : Pine", "Type : Arm Chairs", "Assembly Required : Yes"],
        "Description": "This accent chair with a sturdy arm and enhanced soft cushioning promise to ignite your personal living space."
    }, {
        "Alias": "Curio Cabinet",
        "Name": "Curio Cabinet",
        "Type": "Furniture",
        "Color": "   ",
        "ModelURL": "",
        "Price": 54200,
        "PercentageDiscount": 25,
        "ImagesPath": "  ",
        "Quantity": 0,
        "Specification": [],
        "Description": ""
    }],
    "InfoHotspots": [{
        "Url": "./icons/info.svg"
    }],
    "PanoHotspots": [{
        "Url": "./icons/pano_inner_blue.svg"
    }],
    "InteractiveHotspots": [{
        "Url": "./icons/close.svg"
    }, {
        "Url": "./icons/open.svg"
    }]
}